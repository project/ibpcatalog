<?php

/**
 * @file
 */

/**
 * Prepares variables for ibpcatalog templates.
 *
 * Default template: ibpcatalog.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of content types.
 *
 * @see ibpcatalog()
 */
function template_preprocess_ibpcatalog(&$variables) {
}
